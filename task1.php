<?php
function ConvertFileToPng($file){
    return $file;
}

function ConvertFileToJpg($file){
    return $file;
}

function ConvertFileToTiff($file){
    return $file;
}

function ConvertFileToGiff($file){
    return $file;
}

function ConvertFileToMymft($file){
    return $file;
}

if (isset($_POST['send'])) {
    $result = null;
    if (is_uploaded_file($_FILES['image-file']['tmp_name'])) {
        if ($_POST['select-format'] == 'jpg'){
            $result = ConvertFileToJpg($_FILES['image-file']);
        }
        elseif ($_POST['select-format'] == 'png')
        {
            $result = ConvertFileToPng($_FILES['image-file']);
        }
        elseif ($_POST['select-format'] == 'tiff')
        {
            $result = ConvertFileToTiff($_FILES['image-file']);
        }
        elseif ($_POST['select-format'] == 'giff')
        {
            $result = ConvertFileToGiff($_FILES['image-file']);
        }
        elseif ($_POST['select-format'] == 'mymft')
        {
            $result = ConvertFileToMymft($_FILES['image-file']);
        }

    }
    if (file_exists($result['tmp_name'])) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($result['name']).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($result['size']));
        readfile($result['name']);
        exit;
    }

}

?>
<p> Конвертирования изображений :</p>

<form method="POST" enctype="multipart/form-data">
    <div>
        <label for="image-file">Выбрать изображение</label>
        <input type="file" name="image-file" id="image-file" accept=".png, .jpg, .jpeg, .tiff, .giff "  required="required" >
    </div>
    <label for="select-format">Сохранить в формат:</label>
    <select name="select-format" id="select-format" >
        <option value="jpg">jpg</option>
        <option value="png">png</option>
        <option value="tiff">tiff</option>
        <option value="giff">giff</option>
        <option value="myfmt">myfmt</option>
        <option value="giff">giff</option>
    </select>

    <div>
        <input type="submit" name="send" value="Конвертировать"/>
    </div>
</form>

<?php
//    if (isset($result)): ?>
<!--        <hr />-->
<!--    <p>Ссылка для скачивания.</p>-->
<!--    echo <a href="--><?php //=$result['tmp_name']?><!--"> Скачать </a>;-->
<!---->
<!--    --><?php //endif ?>