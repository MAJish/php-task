<?php
function quadratic_equation($a, $b, $c): ?array
{
    $discriminant = pow($b, 2) - 4 * $a * $c;
    $x2 = null;
    if ($a == 0){
        return null;
    }
    if ($discriminant > 0){
        $x1 = (-$b + sqrt($discriminant)) / (2 * $a);
        $x2 = (-$b - sqrt($discriminant)) / (2 * $a);
    }
    else {
        $x1 = -$b / 2 * $a;
    }
    return array($x1, $x2, $discriminant);
}

if (isset($_POST['send'])) {
    $a = (float)$_POST['a'];
    $b = (float)$_POST['b'];
    $c = (float)$_POST['c'];
    $result = quadratic_equation($a, $b, $c);
}

?>

<p>Введите коэффициенты :</p>
<h4>a * x<sup>2</sup> + b * x + с = 0</h4>
<form method="POST">
    <p>a = <input type="number" name="a" step="any" required="required"/></p>
    <p>b = <input type="number" name="b" step="any" required="required"/></p>
    <p>c = <input type="number" name="c" step="any" required="required"/></p>
    <input type="submit" name="send" value="решить"/>
</form>

<?php

    if (isset($result)): ?>
        <hr />
        <h3><?=$a ?> * x<sup>2</sup> + <?=$b ?> * x + <?=$c ?> = 0</h3>
        <p>Дискриминант равен : <?=$result[2]?></p>
        <?php if (isset($result[1])): ?>
            <p>Корни уравнения :</p>
            <p>X<sub>1</sub> = <?=$result[0] ?></p>
            <p>X<sub>2</sub> = <?=$result[1] ?></p>
        <?php elseif ($result[2] == 0): ?>
             <p>Корень уравнения:</p>
            <p>X<sub>1</sub> = <?=$result[0] ?></p>
        <?php elseif ($result[2] < 0): ?>
            <p>Корней нет</p>
        <?php endif ?>
    <?php else: ?>
        <p>Это не квадратное уровнение</p>
<?php endif ?>
